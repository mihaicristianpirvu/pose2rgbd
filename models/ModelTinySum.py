from neural_wrappers.models import ModelSafeUAVTinySum
from neural_wrappers.pytorch import NeuralNetworkPyTorch

class ModelTinySum(NeuralNetworkPyTorch):
	def __init__(self, dIn, dOut, outputFunc, **k):
		super().__init__(**k)
		self.baseModel = ModelSafeUAVTinySum(dIn=dIn, dOut=dOut, numFilters=16)
		self.outputFunc = outputFunc

	def forward(self, x):
		yBase = self.baseModel(x["position"])
		yOut = self.outputFunc(yBase)
		return yOut
