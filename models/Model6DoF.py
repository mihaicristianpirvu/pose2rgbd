import torch as tr
import torch.nn as nn
import torch.nn.functional as F
from neural_wrappers.pytorch import NeuralNetworkPyTorch

class Model6DoF(NeuralNetworkPyTorch):
	def __init__(self, dIn, dOut, outputFunc, **k):
		super().__init__(**k)
		self.fc1 = nn.Linear(dIn, 4 * 4 * 1024)
		self.convtr1 = nn.ConvTranspose2d(in_channels=1024, out_channels=512, kernel_size=2, stride=2, bias=False)
		self.convtr2 = nn.ConvTranspose2d(in_channels=512, out_channels=256, kernel_size=2, stride=2, bias=False)
		self.convtr3 = nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=2, stride=2, bias=False)
		self.convtr4 = nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=2, stride=2, bias=False)
		self.convtr5 = nn.ConvTranspose2d(in_channels=64, out_channels=32, kernel_size=2, stride=2, bias=False)
		self.convtr6 = nn.ConvTranspose2d(in_channels=32, out_channels=16, kernel_size=2, stride=2, bias=False)
		self.convtr7 = nn.ConvTranspose2d(in_channels=16, out_channels=dOut, kernel_size=2, stride=2)
		self.bn1 = nn.BatchNorm2d(num_features=512)
		self.bn2 = nn.BatchNorm2d(num_features=256)
		self.bn3 = nn.BatchNorm2d(num_features=128)
		self.bn4 = nn.BatchNorm2d(num_features=64)
		self.bn5 = nn.BatchNorm2d(num_features=32)
		self.bn6 = nn.BatchNorm2d(num_features=16)

		self.outputFunc = outputFunc

	def forward(self, x):
		y1 = F.relu(self.fc1(x["position"])).reshape(-1, 1024, 4, 4)
		y2 = F.relu(self.bn1(self.convtr1(y1)))
		y3 = F.relu(self.bn2(self.convtr2(y2)))
		y4 = F.relu(self.bn3(self.convtr3(y3)))
		y5 = F.relu(self.bn4(self.convtr4(y4)))
		y6 = F.relu(self.bn5(self.convtr5(y5)))
		y7 = F.relu(self.bn6(self.convtr6(y6)))
		y8 = self.convtr7(y7).permute(0, 2, 3, 1)
		yOut = self.outputFunc(y8)
		return yOut