import torch as tr
import torch.nn as nn
import torch.nn.functional as F
from neural_wrappers.pytorch import NeuralNetworkPyTorch

class Model6DoFBottleneck(NeuralNetworkPyTorch):
	def __init__(self, numDepthSlices, config=[64, 32, 32, 32, 64, 64]):
		super().__init__()
		self.convtr1 = nn.ConvTranspose2d(in_channels=config[0], out_channels=config[1], kernel_size=2, stride=2)
		self.bn1 = nn.BatchNorm2d(num_features=config[1])
		self.conv2 = nn.Conv2d(in_channels=config[1], out_channels=config[2], kernel_size=3, stride=1, padding=1)
		self.bn2 = nn.BatchNorm2d(num_features=config[2])
		self.conv3 = nn.Conv2d(in_channels=config[2], out_channels=config[3], kernel_size=3, stride=1, padding=1)
		self.bn3 = nn.BatchNorm2d(num_features=config[3])

		self.conv4 = nn.Conv2d(in_channels=config[3] + numDepthSlices, out_channels=config[4], \
			kernel_size=3, stride=1, padding=1)
		self.bn4 = nn.BatchNorm2d(num_features=config[4])
		self.conv5 = nn.Conv2d(in_channels=config[4], out_channels=config[5], kernel_size=3, stride=1, padding=1)
		self.bn5 = nn.BatchNorm2d(num_features=config[5])

	def forward(self, x, yClassifier):
		# Small bottleeck of 512x512x16
		y1 = F.relu(self.bn1(self.convtr1(x)))
		y2 = F.relu(self.bn2(self.conv2(y1)))
		y3 = F.relu(self.bn3(self.conv3(y2)))

		# Concatenate classifier output with features from end of bottleneck and 2 more convs
		y3cat = tr.cat([y3, yClassifier], dim=1)
		y4 = F.relu(self.bn4(self.conv4(y3cat)))
		y5 = F.relu(self.bn5(self.conv5(y4)))
		return y5

class Model6DoFSliceV1(NeuralNetworkPyTorch):
	def __init__(self, dIn, dOut, outputFunc, **k):
		super().__init__(**k)
		self.fc1 = nn.Linear(dIn, 4 * 4 * 1024)
		self.convtr1 = nn.ConvTranspose2d(in_channels=1024, out_channels=512, kernel_size=2, stride=2, bias=False)
		self.convtr2 = nn.ConvTranspose2d(in_channels=512, out_channels=256, kernel_size=2, stride=2, bias=False)
		self.convtr3 = nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=2, stride=2, bias=False)
		self.convtr4 = nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=2, stride=2, bias=False)
		self.convtr5 = nn.ConvTranspose2d(in_channels=64, out_channels=32, kernel_size=2, stride=2, bias=False)
		self.convtr6 = nn.ConvTranspose2d(in_channels=32, out_channels=16, kernel_size=2, stride=2, bias=False)
		self.bn1 = nn.BatchNorm2d(num_features=512)
		self.bn2 = nn.BatchNorm2d(num_features=256)
		self.bn3 = nn.BatchNorm2d(num_features=128)
		self.bn4 = nn.BatchNorm2d(num_features=64)
		self.bn5 = nn.BatchNorm2d(num_features=32)
		self.bn6 = nn.BatchNorm2d(num_features=16)

		numDepthSlices = self.hyperParameters["numDepthSlices"]
		self.convtr7Classifier = nn.ConvTranspose2d(in_channels=16, out_channels=numDepthSlices, kernel_size=2, \
			stride=2, bias=False)
		self.bottleneck = Model6DoFBottleneck(numDepthSlices, config=[16, 16, 16, 16, 16, 16])
		self.convFinal = nn.Conv2d(in_channels=16, out_channels=dOut, kernel_size=3, stride=1, padding=1)

		self.outputFunc = outputFunc

	def forward(self, x):
		y1 = F.relu(self.fc1(x["position"])).reshape(-1, 1024, 4, 4)
		y2 = F.relu(self.bn1(self.convtr1(y1)))
		y3 = F.relu(self.bn2(self.convtr2(y2)))
		y4 = F.relu(self.bn3(self.convtr3(y3)))
		y5 = F.relu(self.bn4(self.convtr4(y4)))
		y6 = F.relu(self.bn5(self.convtr5(y5)))
		y7 = F.relu(self.bn6(self.convtr6(y6)))

		# Classifier that outputs N binary masks for the depth
		y8Classifier = tr.sigmoid(self.convtr7Classifier(y7))
		yBottleneck = self.bottleneck(y7, y8Classifier)

		yFinal = self.convFinal(yBottleneck).permute(0, 2, 3, 1)
		yOut = self.outputFunc(yFinal)
		yOut["depthSlices"] = y8Classifier
		return yOut