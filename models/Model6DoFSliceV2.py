import torch as tr
import torch.nn as nn
import torch.nn.functional as F
from neural_wrappers.pytorch import NeuralNetworkPyTorch
from .Model6DoFSliceV1 import Model6DoFBottleneck

class Model6DoFSliceV2(NeuralNetworkPyTorch):
	def __init__(self, dIn, dOut, outputFunc, **k):
		super().__init__(**k)
		self.fc1 = nn.Linear(dIn, 4 * 4 * 1024)
		self.convtr1 = nn.ConvTranspose2d(in_channels=1024, out_channels=512, kernel_size=2, stride=2, bias=False)
		self.bn1 = nn.BatchNorm2d(num_features=512)
		self.convtr2 = nn.ConvTranspose2d(in_channels=512, out_channels=256, kernel_size=2, stride=2, bias=False)
		self.bn2 = nn.BatchNorm2d(num_features=256)
		self.convtr3 = nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=2, stride=2, bias=False)
		self.bn3 = nn.BatchNorm2d(num_features=128)
		self.convtr4 = nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=2, stride=2, bias=False)
		self.bn4 = nn.BatchNorm2d(num_features=64)

		# Classifier + bottleneck at a quarter of resolution (128x128 for 512x512)
		numDepthSlices = self.hyperParameters["numDepthSlices"]
		self.convtr5Classifier = nn.ConvTranspose2d(in_channels=64, out_channels=numDepthSlices, kernel_size=2, \
			stride=2, bias=False)
		self.bottleneck = Model6DoFBottleneck(numDepthSlices, config=[64, 32, 32, 32, 32, 32])

		self.convtr5 = nn.ConvTranspose2d(in_channels=32, out_channels=32, kernel_size=2, stride=2, bias=False)
		self.bn5 = nn.BatchNorm2d(num_features=32)
		self.convtr6 = nn.ConvTranspose2d(in_channels=32, out_channels=16, kernel_size=2, stride=2, bias=False)
		self.bn6 = nn.BatchNorm2d(num_features=16)

		self.convFinal = nn.Conv2d(in_channels=16, out_channels=dOut, kernel_size=3, stride=1, padding=1)
		self.outputFunc = outputFunc

	def forward(self, x):
		y1 = F.relu(self.fc1(x["position"])).reshape(-1, 1024, 4, 4)
		y2 = F.relu(self.bn1(self.convtr1(y1)))
		y3 = F.relu(self.bn2(self.convtr2(y2)))
		y4 = F.relu(self.bn3(self.convtr3(y3)))
		y5 = F.relu(self.bn4(self.convtr4(y4)))
		# Classifier that outputs N binary masks for the depth
		y5Classifier = tr.sigmoid(self.convtr5Classifier(y5))
		yBottleneck = self.bottleneck(y5, y5Classifier)
		y6 = F.relu(self.bn5(self.convtr5(yBottleneck)))
		y7 = F.relu(self.bn6(self.convtr6(y6)))
		yOut = self.outputFunc(self.convFinal(y7).permute(0, 2, 3, 1))
		yOut["depthSlices"] = y5Classifier
		return yOut
