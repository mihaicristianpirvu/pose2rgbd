import matplotlib.pyplot as plt
import numpy as np

def npGetInfo(data):
	return "Shape: %s. Min: %s. Max: %s. Mean: %s. Std: %s. Dtype: %s" % \
		(data.shape, np.min(data), np.max(data), np.mean(data), np.std(data), data.dtype)

def plotOneItem(data, labels, rgbRenorm, depthRenorm):
	rgb = np.uint8(rgbRenorm(labels["rgb"]))
	depth = depthRenorm(labels["depth"])

	ax = plt.subplots(1, 2)[1]
	[axi.set_axis_off() for axi in ax.ravel()]
	ax[0].imshow(rgb)
	ax[0].set_title("RGB")
	ax[1].imshow(depth, cmap="plasma")
	ax[1].set_title("Depth")

def plotNeighbouringItems(data, labels, rgbRenorm, depthRenorm):
	numNeightbours = len(labels["neighbourPositions"])
	rgb = np.uint8(rgbRenorm(labels["rgb"]))
	neighbourRgbs = np.uint8(rgbRenorm(labels["neighbourRgbs"]))
	depth = depthRenorm(labels["depth"])
	neighbourDepths = depthRenorm(labels["neighbourDepths"])

	ax = plt.subplots(numNeightbours + 1, 2)[1]
	[axi.set_axis_off() for axi in ax.ravel()]
	ax[0, 0].imshow(rgb)
	ax[0, 0].set_title("RGB")
	ax[0, 1].imshow(depth, cmap="plasma")
	ax[0, 1].set_title("Depth")
	for k in range(numNeightbours):
		ax[k + 1, 0].imshow(neighbourRgbs[k])
		ax[k + 1, 0].set_title("Neighbour %d RGB" % (k + 1))
		ax[k + 1, 1].imshow(neighbourDepths[k], cmap="plasma")
		ax[k + 1, 1].set_title("Neighbour %d Depth" % (k + 1))

def plotInput(data, labels):
	all_points = data["position"].shape[-1]
	if all_points > 1:
		ax = plt.subplots(1, all_points)[1]
		for i in range(all_points):
			item = data["position"][..., i]
			ax[i].imshow(item, cmap="gray")
	else:
		plt.figure()
		plt.imshow(data["position"][..., 0], cmap="gray")

def plotDepthSlices(slices):
	ax = plt.subplots(1, len(slices))[1]
	for i in range(len(slices)):
		ax[i].imshow(slices[i], cmap="gray")

def test_dataset(generator, rgbRenorm, depthRenorm):
	for data, labels in generator:
		if "neighbourRgbs" in labels:
			plotFunction = plotNeighbouringItems
		else:
			plotFunction = plotOneItem

		MB = len(labels["rgb"])
		for i in range(MB):
			mbItems = [
				{k : data[k][i] for k in data},
				{k : labels[k][i] for k in labels}
			]
			plotFunction(mbItems[0], mbItems[1], rgbRenorm, depthRenorm)

			if len(data["position"].shape) != 2:
				plotInput(*mbItems)

			if "depthSlices" in labels:
				plotDepthSlices(mbItems[1]["depthSlices"])

			plt.show()