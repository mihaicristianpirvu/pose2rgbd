import numpy as np
import torch.nn.functional as F
import torch as tr

metrics = {
	"diceLoss" : None,
	"bceLoss" : None
}

def lossFn(y, t, lossFunctionsWeights):
	loss = 0
	for key in lossFunctionsWeights:
		lossWeight, lossFunc = lossFunctionsWeights[key]
		loss += lossWeight * lossFunc(y, t)
	return loss

def lossRgb(y, t):
	return ((y["rgb"] - t["rgb"])**2).mean()

def lossDepth(y, t):
	return ((y["depth"] - t["depth"])**2).mean()

def diceLoss(y, t):
	global metrics
	y, t = y["depthSlices"], t["depthSlices"]
	w, h = y.shape[2], y.shape[3]
	t = F.interpolate(t, (h, w), mode="area")

	numerator = 2 * (y * t).sum()
	denominator = (y + t).sum()
	result = 1 - (numerator + 1) / (denominator + 1 + 1e-8)
	metrics["diceLoss"] = result.cpu().detach().numpy()
	return result

def bceLoss(y, t):
	y, t = y["depthSlices"], t["depthSlices"]
	w, h = y.shape[2], y.shape[3]
	t = F.interpolate(t, (h, w), mode="nearest")

	# Binary cross entropy + NaN/Inf checks
	Loss = -(t * tr.log(y + 1e-5) + (1 - t) * tr.log(1 - y + 1e-5)).mean()

	metrics["bceLoss"] = Loss.cpu().detach().numpy()
	return Loss

def diceMetric(y, t, **k):
	global metrics
	return metrics["diceLoss"]

def bceMetric(y, t, **k):
	global metrics
	return metrics["bceLoss"]

def depthMetric(y, t, depthRenorm, **k):
	# Normalize back to meters, using the given function
	yDepthMeters = depthRenorm(y["depth"])
	tDepthMeters = depthRenorm(t["depth"])
	return np.abs(yDepthMeters - tDepthMeters).mean()

def RGBMetricL1Pixel(y, t, rgbRenorm, **k):
	# Remap y and t to [0 : 255] using the given function
	yRgbOrig = rgbRenorm(y["rgb"])
	tRgbOrig = rgbRenorm(t["rgb"])
	return np.abs(yRgbOrig - tRgbOrig).mean()

def RGBMetricL2(y, t, **k):
	return ((y["rgb"] - t["rgb"])**2).mean()
