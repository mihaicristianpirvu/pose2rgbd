import matplotlib.pyplot as plt
import numpy as np
from loss_functions import depthMetric, RGBMetricL1Pixel

def test(model, valGenerator, valNumSteps, Dir, rgbRenorm, depthRenorm):
	imgIx = 0
	for data, labels in valGenerator:
		positions = data["position"]
		depths = labels["depth"]
		rgbs = labels["rgb"]
		MB = len(positions)

		results = model.npForward(data)
		resDepths = results["depth"] if "depth" in results else np.zeros(labels["depth"].shape)
		resRgbs = results["rgb"] if "rgb" in results else np.zeros(labels["rgb"].shape)

		for j in range(MB):
			imgIx += 1
			metric = depthMetric({"depth" : resDepths[j : j + 1]}, {"depth" : depths[j : j+1]}, \
				depthRenorm=depthRenorm)
			pixel = RGBMetricL1Pixel({"rgb" : resRgbs[j : j + 1]}, {"rgb" : rgbs[j : j + 1]}, rgbRenorm=rgbRenorm)
			# print("Error: %2.3f (m)" % (metric))
			rgb = np.uint8(rgbRenorm(rgbs[j]))
			resRgb = np.uint8(rgbRenorm(resRgbs[j]))

			depth = depthRenorm(depths[j])
			resDepth = depthRenorm(resDepths[j])

			_, axarr = plt.subplots(2, 2 + ("depthSlices" in results))
			plt.gcf().set_size_inches(10, 10)
			[axi.set_axis_off() for axi in axarr.ravel()]
			axarr[0, 0].imshow(rgb)
			axarr[0, 1].imshow(depth, cmap="plasma")
			axarr[1, 0].imshow(resRgb)
			axarr[1, 0].set_title("Error: %2.2f (px)" % (pixel))
			axarr[1, 1].imshow(resDepth, cmap="plasma")
			axarr[1, 1].set_title("Error: %2.2f (m)" % (metric))

			if "depthSlices" in results:
				resSlices = results["depthSlices"][j]
				resSlices = resSlices.sum(axis=0)
				axarr[1, 2].imshow(resSlices, cmap="gray")
				axarr[1, 2].set_title("Confidence map")

			# plt.savefig("%s/%d.png" % (Dir, imgIx))
			plt.show()