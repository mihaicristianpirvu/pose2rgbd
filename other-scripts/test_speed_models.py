import sys
sys.path.append("..")
import numpy as np
from datetime import datetime
from models import Model6DoFSliceV1, Model6DoF, Model6DoFSliceV2
from neural_wrappers.pytorch import NeuralNetworkPyTorch
from neural_wrappers.pytorch.pytorch_utils import getNumParams
import torch as tr
import torch.nn as nn
import torch.nn.functional as F
from functools import partial
from neural_wrappers.utilities import RunningMean

device = "cuda" if tr.cuda.is_available() else "cpu"

outputFunc = lambda x : {"depth" : tr.tanh(x[..., 0])}
models = {
	"Model6DoF" : partial(Model6DoF, dIn=7, dOut=1, outputFunc=outputFunc, \
		hyperParameters={"outputRepresentation" : "depth"}),
	"Model6DoFSliceV2-10 " : partial(Model6DoFSliceV2, dIn=7, dOut=1, outputFunc=outputFunc, \
		hyperParameters={"outputRepresentation" : "depth", "numDepthSlices" : 10}),
	"Model6DoFSliceV2-32 " : partial(Model6DoFSliceV2, dIn=7, dOut=1, outputFunc=outputFunc, \
		hyperParameters={"outputRepresentation" : "depth", "numDepthSlices" : 32}),
	"Model6DoFSliceV2-64 " : partial(Model6DoFSliceV2, dIn=7, dOut=1, outputFunc=outputFunc, \
		hyperParameters={"outputRepresentation" : "depth", "numDepthSlices" : 64})
}
models = {k : models[k]().to(device) for k in models}

def computeModelRAM(model, MB):
	import os
	import psutil
	process = psutil.Process(os.getpid())
	before = process.memory_info().rss / (1024**2)
	x = {"position" : tr.randn(MB, 7).to(device)}
	y = model.forward(x)
	after = process.memory_info().rss / (1024**2)
	return after - before

def doExperiment(model, MB, count=100):
	def f(m, x):
		now = datetime.now();
		res = m.npForward(x)
		return 1 / (datetime.now() - now).total_seconds()

	mean = RunningMean()
	x = {"position" : np.random.randn(MB, 7).astype(np.float32)}

	for i in range(10):
		f(model, x)
	for i in range(count):
		mean.update(f(model, x), 1)
	return mean.get() * MB

for k in models:
	print("Model %s (%s). Params: %d." % (k, device, getNumParams(models[k].parameters())[0]))
	for MB in [1, 5, 10]:
		# ram = computeModelRAM(models[k], MB=MB)
		ram = 0
		fps = doExperiment(models[k], MB=MB)
		print("\tMB: %d. FPS: %2.2f. RAM: %2.2f" % (MB, fps, ram))
