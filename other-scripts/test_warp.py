import matplotlib.pyplot as plt
import numpy as np
import sys
import torch as tr
import torch.nn.functional as F
from neural_wrappers.utilities import getGenerators

sys.path.append("..")
from carla_h5_reader import CarlaH5Reader as Reader
from test_dataset import test_dataset

sys.path.append("/home/mihai/Public/Projects/depth-pose-project")
from inverse_warp import cam2pixel, pixel2cam

# img: the source image (where to sample pixels) -- [B, 3, H, W]
# depth: depth map of the target image -- [B, H, W]
# pose: 6DoF pose parameters from target to source -- [B, 6]
# intrinsics: camera intrinsic matrix -- [B, 3, 3]
# intrinsics_inv: inverse of the intrinsic matrix -- [B, 3, 3]
def inverse_warp_mat(img, depth, pose_mat, intrinsics, intrinsics_inv, padding_mode='zeros'):
	proj_cam_to_src_pixel = intrinsics @ pose_mat  # [B, 3, 4]
	cam_coords = pixel2cam(depth, intrinsics_inv)  # [B,3,H,W]

	src_pixel_coords = cam2pixel(cam_coords, proj_cam_to_src_pixel[:,:,:3], proj_cam_to_src_pixel[:,:,-1:], padding_mode)  # [B,H,W,2]
	projected_img = F.grid_sample(img, src_pixel_coords, mode="bilinear", padding_mode=padding_mode)
	return projected_img

def pose2mat(P, seq):
	def rotx(a):
		return np.array([[1, 0, 0],[0, np.cos(a), -np.sin(a)], [0, np.sin(a), np.cos(a)]])

	def roty(a):
		return np.array([[np.cos(a), 0, np.sin(a)], [0, 1, 0], [-np.sin(a), 0, np.cos(a)]])

	def rotz(a):
		return np.array([[np.cos(a), -np.sin(a), 0], [np.sin(a), np.cos(a), 0], [0, 0, 1]])

	def eul2Rot(eul, seq="zyx"):
		seqFcts = {"x" : rotx, "y" : roty, "z": rotz}
		res = seqFcts[seq[0]](eul[0])
		for i in range(1, len(seq)):
			res = res @ seqFcts[seq[i]](eul[i])
		return res

	def fRT(R, t):
		return np.vstack((np.hstack((R,np.array(t).reshape((3, 1)))), [0, 0, 0, 1]))

	t = P[0 : 3]
	R = eul2Rot(P[3 : ], seq=seq)
	return R, t

def getRelativePose(P1, P2, seq):
	R1, t1 = pose2mat(P1, seq)
	R2, t2 = pose2mat(P2, seq)

	R12 = R2.T @ R1
	t12 = (t1 - t2)[[1,2,0]]
	M12 = np.concatenate([R12, np.expand_dims(t12, axis=-1)], axis=-1)
	return M12

reader = Reader(sys.argv[1], inputRepresentation="6dof", numNeighbours=2, resolution=(320, 320))
generator, numSteps, valGenerator, valNumSteps = getGenerators(reader, 5, maxPrefetch=0)

i = 0
for Z, (data, labels) in enumerate(generator):
	if Z == 0:
		continue
	# data, labels = next(generator)
	K = reader.intrinsic

	for j in range(len(labels["rgb"])):
		i += 1
		if i < 15:
			continue
		rgb = labels["rgb"][j]
		neighbourRgb = labels["neighbourRgbs"][j, 1]
		depth = (labels["depth"][j] * 0.5 + 0.5) * 300
		neighbourDepth = (labels["neighbourDepths"][j, 1] * 0.5 + 0.5) * 300
		position = labels["position"][j]
		neighbourPosition = labels["neighbourPositions"][j, 1]

		def f(x):
			return tr.from_numpy(x.astype(np.float32)).unsqueeze(0)
		def g(x):
			return x.numpy()[0]
		def fWarp(rgb, depth, relPose, K):
			return g(inverse_warp_mat(f(rgb.transpose(2, 0, 1)), f(depth), f(relPose), f(K), f(np.linalg.inv(K)))).transpose(1, 2, 0)

		M12 = getRelativePose(position, neighbourPosition, "xzy")
		M21 = getRelativePose(neighbourPosition, position, "xzy")
		# print(M12)
		# print(M21)
		M12[0:3, 0:3] = np.eye(3)
		# M12[:, 3] = [10, 0 ,0]
		M21[0:3, 0:3] = np.eye(3)
		# M21[:, 3] = [0, 0, 0]
		res12 = fWarp(rgb, depth, M21, K)
		res21 = fWarp(neighbourRgb, neighbourDepth, M12, K)

		# print(M21)
		# t21 = [-15.67, 0, -3.04]
		# rel21 = np.array([*t21, 0, 0, 0])
		# res = g(inverse_warp(f(rgb.transpose(2, 0, 1)), f(depth), f(rel21), f(K), f(np.linalg.inv(K)))).transpose(1, 2, 0)
		# print(res.min(), res.max())

		ax = plt.subplots(2, 3)[1]
		ax[0, 0].imshow(rgb * 0.5 + 0.5)
		ax[0, 1].imshow(depth, cmap="plasma")
		ax[0, 2].imshow(res21 * 0.5 + 0.5)
		ax[1, 0].imshow(neighbourRgb * 0.5 + 0.5)
		ax[1, 1].imshow(neighbourDepth, cmap="plasma")
		ax[1, 2].imshow(res12 * 0.5 + 0.5)
		plt.show()